package com.qb.study.hc.thread.chapter2.bio;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author: GeorgeQin
 * Date: 2019-03-10
 * Time: 8:28 PM
 * 第三版：1. net io 不阻塞，2.可以http访问
 * http支持
 */
public class BIOServer3 {
    private static Charset charset = Charset.forName("utf-8");
    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("服务器启动成功");
        while (!serverSocket.isClosed()) {
            Socket request = serverSocket.accept();
            System.out.println("收到新连接：" + request.toString());
            threadPool.execute(()->{
                try {
                    //接收数据，打印
                    InputStream inputStream = request.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
                    String msg;
                    while ((msg = reader.readLine()) != null) {
                        if (msg.length() == 0) {
                            break;
                        }
                        System.out.println("msg:" + msg);
                    }
                    System.out.println("收到数据，来自：" + request.toString());
                    //响应结果 200
                    OutputStream outputStream = request.getOutputStream();
                    outputStream.write("HTTP/1.1 200 OK\r\n".getBytes());
                    outputStream.write("Content-Length: 11\r\n\r\n".getBytes());
                    outputStream.write("hello world!".getBytes());
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        request.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        serverSocket.close();
    }
}
