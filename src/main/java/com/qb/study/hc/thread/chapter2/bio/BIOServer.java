package com.qb.study.hc.thread.chapter2.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Author: GeorgeQin
 * Date: 2019-03-10
 * Time: 7:00 PM
 * 第一版: 1. . IO阻塞，2. 不能提供http访问
 */
public class BIOServer {
    private static Charset charset = Charset.forName("UTF-8");

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("服务器启动成功");
        while (!serverSocket.isClosed()) {
            Socket request = serverSocket.accept();//阻塞
            System.out.println("收到新连接:"+request.toString());
            try {
                //接收数据打印
                InputStream inputStream = request.getInputStream();// net + i/o
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
                String msg;
                while ((msg = reader.readLine()) != null) {// 没有数据，阻塞
                    if (msg.length() == 0) {
                        break;
                    }
                    System.out.println("MSG:" + msg);
                }
                System.out.println("收到数据,来自：" + request.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    request.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        serverSocket.close();
    }

}
