package com.qb.study.hc.thread.chapter2.bio;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Author: GeorgeQin
 * Date: 2019-03-10
 * Time: 7:00 PM
 */
public class BIOClient {
    private static Charset charset = Charset.forName("UTF-8");

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 8080);
        OutputStream out = socket.getOutputStream();
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入：");
        String msg = scanner.nextLine();
        out.write(msg.getBytes(charset));// 阻塞，写完成
        scanner.close();
        socket.close();
    }
}
