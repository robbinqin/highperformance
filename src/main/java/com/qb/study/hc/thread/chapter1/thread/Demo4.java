package com.qb.study.hc.thread.chapter1.thread;

/**
 * Author: GeorgeQin
 * Date: 2019-03-07
 * Time: 8:52 PM
 * 通过状态位来判断线程是否停止
 */
public class Demo4 {
    public volatile static boolean flag = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            try {
                // 判断是否运行
                while (flag) {
                        System.out.println("运行中。。。");
                        Thread.sleep(1000L);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        // 3秒之后，将状态标志改为False，代表不继续运行
        Thread.sleep(3000L);
        flag=false;
        System.out.println("程序运行结束。。。");
    }
}
