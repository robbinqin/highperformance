package com.qb.study.hc.thread.chapter1.thread;

/**
 * Author: GeorgeQin
 * Date: 2019-03-07
 * Time: 1:31 PM
 * 高性能编程专题
 * 第一章 多线程并发编程
 * 第一节 Java程序运行原理分析
 * 1.1.1 Java基础
 * JVM运行时数据区
 * 线程共享部分：方法区，堆内存
 * 线程独占部分：虚拟机栈，本地方法栈，程序计数器
 */
public class Demo1 {
    public int x;

    public int sum(int a, int b) {
        return a+b;
    }

    public static void main(String[] args) {
        Demo1 demo1 = new Demo1();
        demo1.x=3;
        int y=2;
        int z = demo1.sum(demo1.x, y);
        System.out.println("person age is " + z);
    }
}
