package com.qb.study.hc.thread.chapter1.thread;

/**
 * Author: GeorgeQin
 * Date: 2019-03-07
 * Time: 8:36 PM
 * 线程stop强制性中止，破坏线程安全的示例
 */
public class Demo3 {
    public static void main(String[] args) {
        StopThread thread = new StopThread();
        thread.start();
        // 休眠1秒，确保i变量自增成功
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 暂停线程
//          thread.stop(); // 错误的终止
        thread.interrupt();
        while (thread.isAlive()) {
            // 确保线程已经终止
            System.out.println("thread is alive");
        }
        thread.print();
    }
}
