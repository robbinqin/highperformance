package com.qb.study.hc.thread.chapter2.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author: GeorgeQin
 * Date: 2019-03-10
 * Time: 8:27 PM
 * 第二版：1. net IO 不阻塞, 2. 不能http访问
 *  多线程支持
 */
public class BIOServer2 {
    private static ExecutorService threadPool= Executors.newCachedThreadPool();
    private static Charset charset = Charset.forName("utf-8");

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("tomcat server started...");
        while (!serverSocket.isClosed()) {
            Socket request = serverSocket.accept();
            System.out.println("new connection is: " + request.toString());
            threadPool.execute(()->{
                try {
                    // 接收数据、打印
                    InputStream inputStream = request.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
                    String msg;
                    while ((msg = reader.readLine()) != null) {// 阻塞
                        if (msg.length() == 0) {
                            break;
                        }
                        System.out.println("msg:" + msg);
                    }
                    System.out.println("收到的数据：来自：" + request.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        request.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        serverSocket.close();
    }
}
